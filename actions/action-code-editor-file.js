
'use strict';

import Action from 'z-abs-corelayer-client/communication/action';


export class ActionCodeEditorFileGet extends Action {
  constructor(projectId, key, title, path, type) {
    super(projectId, key, title, path, type);
  }
}

export class ActionCodeEditorFileSet extends Action {
  constructor(url, query) {
    super(url, query);
  }
}

export class ActionCodeEditorFileAdd extends Action {
  constructor(projectId, title, path, type) {
    super(projectId, title, path, type);
  }
}

export class ActionCodeEditorFileNew extends Action {
  constructor(projectId, path, name, type, templateName) {
    super(projectId, path, name, type, templateName);
  }
}

export class ActionCodeEditorFileRename extends Action {
  constructor(projectId, path, title, newTitle) {
    super(projectId, path, title, newTitle);
  }
}

export class ActionCodeEditorFileUpdate extends Action {
  constructor(key) {
    super(key);
  }
}

export class ActionCodeEditorFileUpdateAll extends Action {
  constructor(keys) {
    super(keys);
  }
}

export class ActionCodeEditorFileClose extends Action {
  constructor(key) {
    super(key);
  }
}

export class ActionCodeEditorFileRemove extends Action {
  constructor(projectId, key) {
    super(projectId, key);
  }
}

export class ActionCodeEditorFileDelete extends Action {
  constructor(projectId, key) {
    super(projectId, key);
  }
}

export class ActionCodeEditorFileEdit extends Action {
  constructor(projectId, key, content) {
    super(projectId, key, content);
  }
}

export class ActionCodeEditorFileMove extends Action {
  constructor(fromKey, toKey) {
    super(fromKey, toKey);
  }
}

export class ActionCodeEditorFileShowInvisibles extends Action {
  constructor(showInvisibles) {
    super(showInvisibles);
  }
}
