
'use strict';

import Action from 'z-abs-corelayer-client/communication/action';


export class ActionCodeEditorProjectGet extends Action {
  constructor(url, query) {
    super(url, query);
  }
}

export class ActionCodeEditorProjectUpdate extends Action {
  constructor(projectId) {
    super(projectId);
  }
}

export class ActionCodeEditorProjectToggle extends Action {
  constructor(projectId, key, expanded) {
    super(projectId, key, expanded);
  }
}
