
'use strict';

import Action from 'z-abs-corelayer-client/communication/action';


export class ActionCodeEditorFolderGet extends Action {
  constructor(projectId, key, title, path) {
    super(projectId, key, title, path);
  }
}

export class ActionCodeEditorFolderNew extends Action {
  constructor(projectId, path, title, types) {
    super(projectId, path, title, types);
  }
}

export class ActionCodeEditorFolderAdd extends Action {
  constructor(projectId, title, path, types) {
    super(projectId, title, path, types);
  }
}

export class ActionCodeEditorFolderUpdate extends Action {
  constructor(projectId, path, title, newTitle, types) {
    super(projectId, path, title, newTitle, types);
  }
}

export class ActionCodeEditorFolderRemove extends Action {
  constructor(projectId, path, title, key) {
    super(projectId, path, title, key);
  }
}

export class ActionCodeEditorFolderDelete extends Action {
  constructor(projectId, path, title, key) {
    super(projectId, path, title, key);
  }
}
