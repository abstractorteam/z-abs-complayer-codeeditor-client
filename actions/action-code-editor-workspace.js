
'use strict';

import Action from 'z-abs-corelayer-client/communication/action';


export class ActionCodeEditorWorkspaceNew extends Action {
  constructor(appName, workspaceName) {
    super(appName, workspaceName);
  }
}

export class ActionCodeEditorWorkspaceGet extends Action {
  constructor(projectId) {
    super(projectId);
  }
}

export class ActionCodeEditorWorkspaceUpdate extends Action {
  constructor(projectId) {
    super(projectId);
  }
}

