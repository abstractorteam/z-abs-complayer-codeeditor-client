
'use strict';

import { ActionCodeEditorFolderNew, ActionCodeEditorFolderAdd } from './action-code-editor-folder';
import DataAction from 'z-abs-corelayer-client/communication/data-action';


export class DataActionCodeEditorFolderNew extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, `${action.path}/${action.title}`);
  }
}

export class DataActionCodeEditorFolderUpdate extends DataAction {
  constructor(oldPath, newPath, action) {
    super();
    this.addRequest(action, oldPath, newPath);
  }
}

export class DataActionCodeEditorFolderDelete extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, `${action.path}/${action.title}`);
  }
}
