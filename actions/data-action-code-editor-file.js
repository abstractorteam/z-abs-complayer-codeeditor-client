
'use strict';

import { ActionCodeEditorFileGet, ActionCodeEditorFileUpdate } from './action-code-editor-file';
import DataAction from 'z-abs-corelayer-client/communication/data-action';


export class DataActionCodeEditorFileGet extends DataAction {
  constructor(file, projectId, key, title, path, type) {
    super();
    this.addRequest(new ActionCodeEditorFileGet(projectId, key, title, path, type), file);
  }
}

export class DataActionCodeEditorFileNew extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.path, action.name, action.type, action.templateName);
  }
}

export class DataActionCodeEditorFileRename extends DataAction {
  constructor(oldPath, newPath, action) {
    super();
    this.addRequest(action, oldPath, newPath);
  }
}

export class DataActionCodeEditorFileUpdate extends DataAction {
  constructor(key, title, path, content) {
    super();
    this.addRequest(new ActionCodeEditorFileUpdate(key), `${path}/${title}`, content);
  }
}

export class DataActionCodeEditorFileDelete extends DataAction {
  constructor(file, action) {
    super();
    this.addRequest(action, file);
  }
}
