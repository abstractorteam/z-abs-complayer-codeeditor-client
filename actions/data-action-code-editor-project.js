
'use strict';

import { ActionCodeEditorProjectGet, ActionCodeEditorProjectUpdate } from './action-code-editor-project';
import DataAction from 'z-abs-corelayer-client/communication/data-action';


export class DataActionCodeEditorProjectGet extends DataAction {
  constructor(url, query) {
    super();
    this.addRequest(new ActionCodeEditorProjectGet(url, query));
  }
}

export class DataActionCodeEditorProjectUpdate extends DataAction {
  constructor(projectId, content) {
    super();
    this.addRequest(new ActionCodeEditorProjectUpdate(projectId), content);
  }
}
