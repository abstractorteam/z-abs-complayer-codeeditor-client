
'use strict';

import { ActionCodeEditorWorkspaceNew, ActionCodeEditorWorkspaceGet, ActionCodeEditorWorkspaceUpdate } from './action-code-editor-workspace';
import DataAction from 'z-abs-corelayer-client/communication/data-action';


export class DataActionCodeEditorWorkspaceGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionCodeEditorWorkspaceGet());
  }
}

export class DataActionCodeEditorWorkspaceNew extends DataAction {
  constructor(appName, workspaceName) {
    super();
    this.addRequest(new ActionCodeEditorWorkspaceNew(appName, workspaceName), appName, workspaceName);
  }
}
