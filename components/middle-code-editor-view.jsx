
'use strict';

import CodeEditorStore from '../stores/code-editor-store';
import LogType from 'AppLayer/cs/log/log-type';
import { ActionCodeEditorFileGet, ActionCodeEditorFileSet, ActionCodeEditorFileEdit, ActionCodeEditorFileClose, ActionCodeEditorFileMove } from '../actions/action-code-editor-file';
import Tabs from 'z-abs-complayer-bootstrap-client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/tab';
import CodeMirrorEditor from 'z-abs-complayer-codemirror-client/code-mirror-editor';
import {RouterContext} from 'z-abs-complayer-router-client/project/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/react-component/react-component-store';
import ReactDOM from 'react-dom';
import React from 'react';


export default class MiddleCodeEditorView extends ReactComponentStore {
  constructor(props) {
    super(props, [CodeEditorStore]);
    this.codemirrorRef = new Map();
    this.close = false;
    this.boundResize = this._resize.bind(this);
  }
  
  didMount() {
    if(undefined !== this.props._uriPath && 0 < this.props._uriPath.length) {
      this.dispatch(CodeEditorStore, new ActionCodeEditorFileSet(`.${this.props._uriPath}`, this.props.location.search));
    }
    else {
      this.dispatch(CodeEditorStore, new ActionCodeEditorFileSet());
    }
    window.addEventListener('resize', this.boundResize);
    this._resize();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
      || !this.shallowCompare(this.state.CodeEditorStore.files, nextState.CodeEditorStore.files)
      || !this.shallowCompare(this.state.CodeEditorStore.current, nextState.CodeEditorStore.current)
      || !this.shallowCompare(this.state.CodeEditorStore.showInvisibles, nextState.CodeEditorStore.showInvisibles);
  }
  
  didUpdate() {
    this._resize();
  }
  
  willUnmount() {
    window.removeEventListener('resize', this.boundResize);
  }
  
  _resize() {
    const ref = this.refs['code_editor_tabs'];
    if(undefined !== ref && null !== this.state.CodeEditorStore.current.file) {
      const tabs = ReactDOM.findDOMNode(ref);
      const navigation = tabs.firstChild;
      const contentHeight = tabs.parentNode.clientHeight - navigation.clientHeight;
      const cmCurrent = this.codemirrorRef.get('cm' + this.state.CodeEditorStore.current.file.key);
      cmCurrent && cmCurrent.setHeight(contentHeight - 9);
    }
  }
  
  renderTab(options) {
    const result = [];
    const queryState = this.state.CodeEditorStore.current.query;  
    const query = !queryState ? undefined : {
      url: queryState.url,
      line: queryState.line,
      typeCss: LogType.csses[Number.parseInt(this.state.CodeEditorStore.current.query.type)]
    };
    this.state.CodeEditorStore.files.forEach((file, key) => {
      result.push(
        <Tab eventKey={key} key={key}
          title={
            <div>
              {file.title}
              <button type="button" className="close middle_code_editor_close" aria-label="Close"
                onClick={(e, a) => {
                  this.close = true;
                  this.dispatch(CodeEditorStore, new ActionCodeEditorFileClose(key));
                }}
              >
                ×
              </button>
            </div>
          }
          onMove={(fromKey, toKey) => {
            this.dispatch(CodeEditorStore, new ActionCodeEditorFileMove(fromKey, toKey));
          }}
        >
          <CodeMirrorEditor ref={(c) => this.codemirrorRef.set('cm' + key, c)} projectId={file.projectId} docKey={key} code={file.content} options={this.getOptions(file.type)} currentType={this.state.CodeEditorStore.current.type} currentFile={this.state.CodeEditorStore.current.file} currentQuery={query}
            onChanged={(projectId, docKey, code) => {
              this.dispatch(CodeEditorStore, new ActionCodeEditorFileEdit(projectId, docKey, code));
            }}
            onGutterChange={(projectId, docKey, lineNbr) => {
              let a = this.state.CodeEditorStore.files.get(docKey);
            }}
            onFocusChange={(focused, docKey) => {
              if(focused) {
                let file = this.state.CodeEditorStore.files.get(docKey);
                if(undefined !== file) {
                  this.dispatch(CodeEditorStore, new ActionCodeEditorFileGet(file.projectId, file.key, file.title, file.path, file.type));
                }
              }
            }}
          />
        </Tab>
      );
    });
    return result;
  }

  renderTabs(options) {
    if(0 !== this.state.CodeEditorStore.files.size) {
      return (
        <Tabs id="code_editor_tabs" ref={'code_editor_tabs'} draggable="true" activeKey={null !== this.state.CodeEditorStore.current.file ? this.state.CodeEditorStore.current.file.key : null}
          onSelect={(selectedKey) => {
            if(!this.close) {
              const file = this.state.CodeEditorStore.files.get(selectedKey);
              this.dispatch(CodeEditorStore, new ActionCodeEditorFileGet(file.projectId, file.key, file.title, file.path, file.type));
            }
            else {
              this.close = false;
            }
          }}
        >
          {this.renderTab(options)}
        </Tabs>
      );
    }
    else {
      return null;
    }
  }

  getOptions(type) {
    let mode = 'text/plain';
    switch(type) {
      case 'css':
        mode = 'text/css';
        break;
      case 'scss':
        mode = 'text/x-scss';
        break;
      case 'less':
        mode = 'text/x-less';
        break;
      case 'jsx':
        mode = 'text/jsx';
        break;
      case 'js':
        mode = 'text/javascript';
        break;
      case 'json':
        mode = 'application/json';
        break;
      case 'html':
        mode = 'text/html';
        break;
      default:
    }
    return {
      autoCloseBrackets: true,
      autofocus: true,
      dragDrop: false,
      extraKeys: {"Ctrl-Space": "autocomplete"},
      fixedGutter: false,
      indentUnit: 2,
      lineNumbers: true,
      matchBrackets: true,
      maxInvisibles: 16,
      mode: mode,
      showInvisibles: this.state.CodeEditorStore.showInvisibles,
      tabSize: 2
    };
  }
  
  render () {
    return (
      <div className="middle_code_editor_view middle_code_editor_view_code">
        {this.renderTabs()}
      </div>
    );
  }
}


MiddleCodeEditorView.contextType = RouterContext;
MiddleCodeEditorView.REMOVE_KEY_LENGTH = 'code_editor_tabs-tab-'.length;
