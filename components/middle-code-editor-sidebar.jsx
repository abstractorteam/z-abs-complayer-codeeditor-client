
'use strict';

import CodeEditorStore from '../stores/code-editor-store';
import { ActionCodeEditorFileGet } from '../actions/action-code-editor-file';
import { ActionCodeEditorFolderGet } from '../actions/action-code-editor-folder';
import { ActionCodeEditorProjectToggle } from '../actions/action-code-editor-project';
import Tree from 'z-abs-complayer-tree-client/tree';
import {RouterContext} from 'z-abs-complayer-router-client/project/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/react-component/react-component-store';
import React from 'react';


export default class MiddleCodeEditorSidebar extends ReactComponentStore {
  constructor(props) {
    super(props, [CodeEditorStore]);
    this.folderIcons = new Map();
    this.folderIcons.set('actorjs', {open: 'images/icon/actor.ico', closed: 'images/icon/actor.ico'});
    this.folderIcons.set('css', {open: 'images/icon/folder_css_open.ico', closed: 'images/icon/folder_css_closed.ico'});
    this.folderIcons.set('html', {open: 'images/icon/folder_html_open.ico', closed: 'images/icon/folder_html_closed.ico'});
    this.folderIcons.set('js', {open: 'images/icon/folder_js_open.ico', closed: 'images/icon/folder_js_closed.ico'});
    this.folderIcons.set('json', {open: 'images/icon/folder_json_open.ico', closed: 'images/icon/folder_json_closed.ico'});
    this.folderIcons.set('jsx', {open: 'images/icon/folder_jsx_open.ico', closed: 'images/icon/folder_jsx_closed.ico'});
    this.folderIcons.set('nodejs', {open: 'images/icon/folder_nodejs_open.ico', closed: 'images/icon/folder_nodejs_closed.ico'});
    this.folderIcons.set('react', {open: 'images/icon/folder_react_open.ico', closed: 'images/icon/folder_react_closed.ico'});
    this.fileIcons = new Map();
    this.fileIcons.set('css', {valid: 'images/icon/css.ico', invalid: 'images/icon/css.ico'});
    this.fileIcons.set('js', {valid: 'images/icon/js.ico', invalid: 'images/icon/js_invalid.ico'});
    this.fileIcons.set('json', {valid: 'images/icon/json.ico', invalid: 'images/icon/json.ico'});
    this.fileIcons.set('jsx', {valid: 'images/icon/jsx.ico', invalid: 'images/icon/jsx.ico'});
    this.fileIcons.set('html', {valid: 'images/icon/html.ico', invalid: 'images/icon/html.ico'});
    this.fileIcons.set('md', {valid: 'images/icon/html.ico', invalid: 'images/icon/html.ico'});
  }

  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
      || !this.shallowCompare(this.state.CodeEditorStore.project, nextState.CodeEditorStore.project)
      || !this.shallowCompare(this.state.CodeEditorStore.current, nextState.CodeEditorStore.current);
  }
  
  render() {
    let stylePanel = {
      height:'100%'
    };
    return (
      <div className="middle_sidebar middle_code_editor_sidebar">
        <div className="panel panel-default" style={stylePanel}>
          <div className="panel_body_sidebar">
            <div>
              <Tree name={'local'} key={this.state.CodeEditorStore.project.projectId} id="code_editor_fancy_tree_code" project={this.state.CodeEditorStore.project} current={this.state.CodeEditorStore.current} folderIcons={this.folderIcons} fileIcons={this.fileIcons}
                onClickFile={(key, title, path, type) => {
                  this.dispatch(CodeEditorStore, new ActionCodeEditorFileGet(this.state.CodeEditorStore.project.projectId, key, title, path, type));
                }}
                onClickFolder={(key, title, path) => {
                  this.dispatch(CodeEditorStore, new ActionCodeEditorFolderGet(this.state.CodeEditorStore.project.projectId, key, title, path));
                }}
                onToggleFolder={(key, expanded) => {
                  this.dispatch(CodeEditorStore, new ActionCodeEditorProjectToggle(this.state.CodeEditorStore.project.projectId, key, expanded));
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MiddleCodeEditorSidebar.contextType = RouterContext;
