
'use strict';

import CodeEditorStore from '../stores/code-editor-store';
import MiddleCodeEditorToolbar from './middle-code-editor-toolbar';
import MiddleCodeEditorSidebar from './middle-code-editor-sidebar';
import MiddleCodeEditorView from './middle-code-editor-view';
import Route from 'z-abs-complayer-router-client/project/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class MiddleCodeEditor extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: this.props._uriPath,
        text: this.props._uriPath ? this.props._uriPath : 'Code Editor',
        prefix: 'CE: '
      };
    });
  }
    
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  render() {
    return (
      <div className="middle middle_code_editor">
        <Route switch="*" handler={MiddleCodeEditorToolbar} />
        <Route switch="*" handler={MiddleCodeEditorSidebar} />
        <Route switch="*" handler={MiddleCodeEditorView} />
      </div>
    );
  }
}
