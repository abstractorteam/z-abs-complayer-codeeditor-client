
'use strict';

import CodeEditorStore from '../stores/code-editor-store';
import { ActionCodeEditorWorkspaceNew } from '../actions/action-code-editor-workspace';
import { ActionCodeEditorProjectUpdate } from '../actions/action-code-editor-project';
import { ActionCodeEditorFolderNew, ActionCodeEditorFolderAdd, ActionCodeEditorFolderUpdate, ActionCodeEditorFolderRemove, ActionCodeEditorFolderDelete } from '../actions/action-code-editor-folder';
import { ActionCodeEditorFileNew, ActionCodeEditorFileAdd, ActionCodeEditorFileRename, ActionCodeEditorFileUpdate, ActionCodeEditorFileUpdateAll, ActionCodeEditorFileRemove, ActionCodeEditorFileDelete, ActionCodeEditorFileShowInvisibles } from '../actions/action-code-editor-file';
import ModalDialogWorkspaceNew from 'z-abs-complayer-modaldialog-client/modal-dialog-workspace-new';
import ModalDialogFolderAdd from 'z-abs-complayer-modaldialog-client/modal-dialog-folder-add';
import ModalDialogFolderNew from 'z-abs-complayer-modaldialog-client/modal-dialog-folder-new';
import ModalDialogFolderProperties from 'z-abs-complayer-modaldialog-client/modal-dialog-folder-properties';
import ModalDialogFolderRemove from 'z-abs-complayer-modaldialog-client/modal-dialog-folder-remove';
import ModalDialogFileAdd from 'z-abs-complayer-modaldialog-client/modal-dialog-file-add';
import ModalDialogFileNew from 'z-abs-complayer-modaldialog-client/modal-dialog-file-new';
import ModalDialogFileProperties from 'z-abs-complayer-modaldialog-client/modal-dialog-file-properties';
import ModalDialogFileRemove from 'z-abs-complayer-modaldialog-client/modal-dialog-file-remove';
import ComponentBootstrapButton from 'z-abs-complayer-bootstrap-client/button';
import { ActionDialogFileGet, ActionDialogFileSet } from 'z-abs-corelayer-client/actions/action-dialog-file';
import DialogFileStore from 'z-abs-corelayer-client/stores/dialog-file-store';
import ReactComponentRealtime from 'z-abs-corelayer-client/react-component/react-component-realtime';
import React from 'react';


export default class MiddleCodeEditorToolbar extends ReactComponentRealtime {
  constructor(props) {
    super(props, [CodeEditorStore, DialogFileStore]);
    this._modalDialogWorkspaceNew = null;
    this._modalDialogFolderNew = null;
    this._modalDialogFolderAdd = null;
    this._modalDialogFolderProperties = null;
    this._modalDialogFolderRemove = null;
    this._modalDialogFileNew = null;
    this._modalDialogFileAdd = null;
    this._modalDialogFileProperties = null;
    this._modalDialogFileRemove = null;
    this.result = {};
    this.boundKeyDown = this._keyDown.bind(this);
    this.buttonFileSaveDisabled = true;
    this.buttonFileSaveAllDisabled = true;
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.CodeEditorStore.project, nextState.CodeEditorStore.project)
      || !this.shallowCompare(this.state.CodeEditorStore.current, nextState.CodeEditorStore.current)
      || !this.shallowCompare(this.state.CodeEditorStore.files, nextState.CodeEditorStore.files)
      || !this.shallowCompare(this.state.CodeEditorStore.showInvisibles, nextState.CodeEditorStore.showInvisibles)
      || !this.shallowCompare(this.state.DialogFileStore.project, nextState.DialogFileStore.project)
      || !this.shallowCompare(this.state.DialogFileStore.current, nextState.DialogFileStore.current)
      || !this.shallowCompare(this.state.DialogFileStore.original, nextState.DialogFileStore.original);
  }

  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _fileSave() {
    this.dispatch(CodeEditorStore, new ActionCodeEditorFileUpdate(this.state.CodeEditorStore.current.file.key));
  }

  _fileSaveAll() {
    const keys = [];
    this.state.CodeEditorStore.files.forEach((file) => { 
      if(file.codeChanged) {
        keys.push(file.key);
      }
    });
    this.dispatch(CodeEditorStore, new ActionCodeEditorFileUpdateAll(keys));
  }
  
  _keyDown(e) {
    if(e.ctrlKey && e.shiftKey && 'S' === e.key) {
      if(!this.buttonFileSaveAllDisabled) {
        e.preventDefault();
        this._fileSaveAll();
      }
    }
    if(e.ctrlKey && 's' === e.key) {
      if(!this.buttonFileSaveDisabled) {
        e.preventDefault();
        this._fileSave();
      }
    }
  }
  
  renderButtonNewWorkspace() {
    let disabled = true;
    this.state.CodeEditorStore.files.forEach((file) => { 
      disabled = disabled && !file.codeChanged;
    });
    disabled = !disabled || !this.state.CodeEditorStore.project.isSaved();
    return (
      <ComponentBootstrapButton placement="bottom" heading="New" content="Workspace" disabled={disabled}
        onClick={(e) => {
          this._modalDialogWorkspaceNew.show(this.state.CodeEditorStore.current.folder);
        }}
      >
        <span className="glyphicon glyphicon_workspace glyphicon-briefcase" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonOpenWorkspace() {
    let disabled = true;
    this.state.CodeEditorStore.files.forEach((file) => { 
      disabled = disabled && !file.codeChanged;
    });
    disabled = !disabled || !this.state.CodeEditorStore.project.isSaved();
    return (
      <ComponentBootstrapButton placement="bottom" heading="Open" content="Workspace" disabled={disabled}
        onClick={(e) => {
        //  if(!this.state.CodeEditorStore.project.isSaved()) {
        //    this.dispatch(CodeEditorStore, new ActionCodeEditorProjectUpdate(this.state.CodeEditorStore.project.projectId));
        //  }
        }}
      >
        <span className="glyphicon glyphicon_workspace glyphicon-briefcase" aria-hidden="true"></span>
        <span className="glyphicon glyphicon_workspace glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonSaveWorkspace() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Save" content="Workspace" disabled={this.state.CodeEditorStore.project.isSaved()}
        onClick={(e) => {
        //  if(!this.state.CodeEditorStore.project.isSaved()) {
        //    this.dispatch(CodeEditorStore, new ActionCodeEditorProjectUpdate(this.state.CodeEditorStore.project.projectId));
        //  }
        }}
      >
        <span className="glyphicon glyphicon_workspace glyphicon-briefcase" aria-hidden="true"></span>
        <span className="glyphicon glyphicon_workspace glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonSaveProject() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Save" content="Project" disabled={this.state.CodeEditorStore.project.isSaved()}
        onClick={(e) => {
          if(!this.state.CodeEditorStore.project.isSaved()) {
            this.dispatch(CodeEditorStore, new ActionCodeEditorProjectUpdate(this.state.CodeEditorStore.project.projectId));
          }
        }}
      >
        <span className="glyphicon glyphicon_project glyphicon-credit-card" aria-hidden="true"></span>
        <span className="glyphicon glyphicon_project glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </ComponentBootstrapButton>
    );
  }

  renderButtonFolderNew() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="New" content="Folder" disabled={'folder' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          this._modalDialogFolderNew.show(this.state.CodeEditorStore.current.folder);
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFolderAdd() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Add" content="Folder" disabled={'folder' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          const folder = this.state.CodeEditorStore.current.folder;
          const dir = `${folder.data.path}/${folder.title}`;
          const project = this.state.CodeEditorStore.project;
          this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'folder'));
          this._modalDialogFolderAdd.show(this.state.CodeEditorStore.current.folder);
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFolderProperties() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Properties" content="Folder" disabled={'folder' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          this._modalDialogFolderProperties.show(this.state.CodeEditorStore.current.folder);
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-wrench" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFolderDelete() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Remove or Delete" content="Folder" disabled={'folder' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          this._modalDialogFolderRemove.show(this.state.CodeEditorStore.current.folder);
        }}
      >
        <span className="glyphicon glyphicon-trash" aria-hidden="true" style={{left: 3}}></span>
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true" style={{left: -17, width: 0, transform: 'scale(0.6)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFileNew() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="New" content="File" disabled={'folder' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          this._modalDialogFileNew.show(this.state.CodeEditorStore.current.folder);
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFileAdd() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Add" content="File" disabled={'folder' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          const folder = this.state.CodeEditorStore.current.folder;
          const dir = `${folder.data.path}/${folder.title}`;
          const project = this.state.CodeEditorStore.project;
          this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'file'));
          this._modalDialogFileAdd.show(this.state.CodeEditorStore.current.folder);
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFileSave() {
    this.buttonFileSaveDisabled = 'file' !== this.state.CodeEditorStore.current.type || !this.state.CodeEditorStore.current.file.codeChanged;
    return (
      <ComponentBootstrapButton placement="bottom" heading="Save" content="File" shortcut="Ctrl+S" disabled={this.buttonFileSaveDisabled}
        onClick={(e) => {
          this._fileSave();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFileSaveAll() {
    this.buttonFileSaveAllDisabled = true;
    this.state.CodeEditorStore.files.forEach((file) => { 
      this.buttonFileSaveAllDisabled = this.buttonFileSaveAllDisabled && !file.codeChanged;
    });
    return (
      <ComponentBootstrapButton placement="bottom" heading="Save" content="All Files" shortcut="Ctrl+Shift+S" disabled={this.buttonFileSaveAllDisabled}
        onClick={(e) => {
          this._fileSaveAll();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top: -1, left: -4, transform: 'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top: 3, left: -10, width: 0, transform: 'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFileProperties() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Properties" content="File" disabled={'file' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          this._modalDialogFileProperties.show(this.state.CodeEditorStore.current.file);
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-wrench" aria-hidden="true" style={{top:'6px',left:'-4px',width:'0px'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFileDelete() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Remove or Delete" content="File" disabled={'file' !== this.state.CodeEditorStore.current.type && 'invalid_file' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          this._modalDialogFileRemove.show(this.state.CodeEditorStore.current.file, true, 'invalid_file' !== this.state.CodeEditorStore.current.type);
        }}
      >
        <span className="glyphicon glyphicon-trash" aria-hidden="true" style={{left: 3}}></span>
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{left: -17, width: 0, transform: 'scale(0.6)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderButtonFileSearch() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Search" content="File" disabled={'file' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-search" aria-hidden="true" style={{top:'0px',left:'4px',width:'0px',transform:'scale(0.9) rotate(90deg)'}}></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderShowInvisiblesButton() {
    return (
      <ComponentBootstrapButton placement="bottom" heading="Show" content="Invisibles" disabled={'file' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileShowInvisibles(!this.state.CodeEditorStore.showInvisibles));
        }}
      >
        <span className="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderModalDialogWorkspaceNew() {
    return (
      <ModalDialogWorkspaceNew ref={(c) => this._modalDialogWorkspaceNew = c} heading="New Workspace" result={'result'}
        onWorkspaceNew={(appName, workspaceName) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorWorkspaceNew(appName, workspaceName));
        }}
        onClear={() => {}}
      />
    );
  }
  
  renderModalDialogFolderNew() {
    return (
      <ModalDialogFolderNew ref={(c) => this._modalDialogFolderNew = c} heading="New Folder" result={'result'}
        onFolderNew={(projectId, key, title, types) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderNew(projectId, key, title, types));
        }}
        onClear={() => {}}
      />
    );
  }
  
  renderModalDialogFolderAdd() {
    let folder = this.state.CodeEditorStore.current.folder;
    let projectId = folder !== null ? folder.projectId : null;
    return (
      <ModalDialogFolderAdd ref={(c) => this._modalDialogFolderAdd = c} heading="Add Folder" result={'result'} project={this.state.DialogFileStore.project} current={this.state.DialogFileStore.current} original={this.state.DialogFileStore.original}
        onAdd={(projectId, title, path, types) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderAdd(projectId, title, path, types));
        }}
        onChoose={(path) => {
          this.dispatch(DialogFileStore, new ActionDialogFileSet(path));
        }}
        onClear={() => {}}
      />
    );
  }
  
  renderModalDialogFolderProperties() {
    return (
      <ModalDialogFolderProperties ref={(c) => this._modalDialogFolderProperties = c} heading="Folder Properties" result={'result'}
        onFolderProperties={(projectId, path, title, newTitle, types) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderUpdate(projectId, path, title, newTitle, types));
        }}
        onClear={() => {}}
      />
    );
  }
  
  renderModalDialogFolderRemove() {
    return (
      <ModalDialogFolderRemove ref={(c) => this._modalDialogFolderRemove = c} heading="Remove or Delete Folder" result={'result'}
        onFolderRemove={(projectId, path, title, key) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderRemove(projectId, path, title, key));
        }}
        onFolderDelete={(projectId, path, title, key) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderDelete(projectId, path, title, key));
        }}
        onClear={() => {}}
      />
    );
  }

  renderModalDialogFileNew() {
    return (
      <ModalDialogFileNew ref={(c) => this._modalDialogFileNew = c} heading="New Code File" result={'result'} templateNames={MiddleCodeEditorToolbar.TEMPLATE_NAMES}
        onFileNew={(projectId, path, title, type, templateName) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileNew(projectId, path, title, type, templateName));
        }}
        onClear={() => {}}
      />
    );
  }

  renderModalDialogFileAdd() {
    let folder = this.state.CodeEditorStore.current.folder;
    let projectId = folder !== null ? folder.projectId : null;
    return (
      <ModalDialogFileAdd ref={(c) => this._modalDialogFileAdd = c} heading="Add Code File" result={this.result} project={this.state.DialogFileStore.project} current={this.state.DialogFileStore.current} original={this.state.DialogFileStore.original}
        onAdd={(projectId, title, path, type) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileAdd(projectId, title, path, type));
        }}
        onChoose={(path) => {
          this.dispatch(DialogFileStore, new ActionDialogFileSet(path));
        }}
        onClear={() => {}}
      />
    );
  }  
  
  renderModalDialogFileProperties() {
    return (
      <ModalDialogFileProperties ref={(c) => this._modalDialogFileProperties = c} heading="Code File Properties" result={'result'}
        onFileProperties={(projectId, path, title, newTitle) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileRename(projectId, path, title, newTitle));
        }}
        onClear={() => {}}
      />
    );
  }
  
  renderModalDialogFileRemove() {
    return (
      <ModalDialogFileRemove ref={(c) => this._modalDialogFileRemove = c} heading="Remove or Delete Code File" result={'result'}
        onFileRemove={(projectId, key) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileRemove(projectId, key));
        }}
        onFileDelete={(projectId, key) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileDelete(projectId, key));
        }}
        onClear={() => {}}
      />
    );
  }
  
  render() {
    return (
      <div className="middle_toolbar middle_code_editor_toolbar">
        {this.renderModalDialogWorkspaceNew()}
        {this.renderModalDialogFolderNew()}
        {this.renderModalDialogFolderAdd()}
        {this.renderModalDialogFolderProperties()}
        {this.renderModalDialogFolderRemove()}
        {this.renderModalDialogFileNew()}
        {this.renderModalDialogFileAdd()}
        {this.renderModalDialogFileProperties()}
        {this.renderModalDialogFileRemove()}
        <div className="btn-toolbar" role="toolbar" aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonNewWorkspace()}
            {this.renderButtonOpenWorkspace()}
            {this.renderButtonSaveWorkspace()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonSaveProject()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonFolderNew()}
            {this.renderButtonFolderAdd()}
            {this.renderButtonFolderProperties()}
            {this.renderButtonFolderDelete()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonFileNew()}
            {this.renderButtonFileAdd()}
            {this.renderButtonFileSave()}
            {this.renderButtonFileSaveAll()}
            {this.renderButtonFileProperties()}
            {this.renderButtonFileDelete()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonFileSearch()}
            {this.renderShowInvisiblesButton()}
          </div>
        </div>
      </div>
    );
  }
}


MiddleCodeEditorToolbar.TEMPLATE_NAMES = ['Class [class js file]', 'None [empty js file]', 'React Base', 'React Store', 'React Realtime'];
