
'use strict';

import { ActionCodeEditorProjectGet } from '../actions/action-code-editor-project';
import { DataActionCodeEditorWorkspaceGet, DataActionCodeEditorWorkspaceNew } from '../actions/data-action-code-editor-workspace';
import { DataActionCodeEditorProjectGet, DataActionCodeEditorProjectUpdate } from '../actions/data-action-code-editor-project';
import { DataActionCodeEditorFileGet, DataActionCodeEditorFileNew, DataActionCodeEditorFileRename, DataActionCodeEditorFileUpdate, DataActionCodeEditorFileDelete } from '../actions/data-action-code-editor-file';
import { DataActionCodeEditorFolderNew, DataActionCodeEditorFolderUpdate, DataActionCodeEditorFolderDelete } from '../actions/data-action-code-editor-folder';
import StoreBaseRealtime from 'z-abs-corelayer-client/store/store-base-realtime';
import Project from 'z-abs-corelayer-cs/project';


class CodeEditorStore extends StoreBaseRealtime {
  constructor() {
    super({
      workspace: new Map(),
      settings: null,
      project: new Project(),
      files: new Map(),
      current: {
        file: null,
        folder: null,
        type: '',
        query: null
      },
      build: {
        errors: []
      },
      showInvisibles: false
    });
  }

  onInit() {
    this.sendDataAction(new DataActionCodeEditorWorkspaceGet());
  }
  
  onActionCodeEditorWorkspaceNew(action) {
    this.sendDataAction(new DataActionCodeEditorWorkspaceNew(action.appName, action.workspaceName));
  }
  
  onActionCodeEditorFileGet(action) {
    const file = this.state.files.get(action.key);
    if(undefined === file) {
      this.sendDataAction(new DataActionCodeEditorFileGet(`${action.path}/${action.title}`, action.projectId, action.key, action.title, action.path, action.type));
    }
    else {
      this.updateState({current: {file: {$set: file}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FILE}}});
      action.history(this._generateQuery(action.path, action.title), {replace: true});
    }
  }

  onActionCodeEditorFileSet(action) {
    if(undefined !== action.url) {
      if(undefined !== action.query) {
        this.updateState({current: {query: {$set: this._createQuery(action.url, action.query)}}});
      }
      const node = this._getNode(action.url);
      if(undefined !== node) {
        if(!node.node.folder) {
          const file = this.state.files.get(node.node.key);
          if(undefined === file) {
            this.sendDataAction(new DataActionCodeEditorFileGet(action.url, node.projectId, node.node.key, node.node.title, node.node.data.path, node.node.data.type));
          }
          else {
            this.updateState({current: {file: {$set: file}}});
            this.updateState({current: {type: {$set: CodeEditorStore.FILE}}});
          }
        }
        else {
          const folder = this._createFolder(node.projectId, node.node);
          this.updateState({current: {folder: {$set: folder}}});
          this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
        }
      }
      else {
        this.sendDataAction(new DataActionCodeEditorProjectGet(action.url, action.query));
      }
    }
    else {
      this.sendDataAction(new DataActionCodeEditorProjectGet());
      this.updateState({current: {file: {$set: null}}});
      this.updateState({current: {folder: {$set: null}}});
      this.updateState({current: {type: {$set: ''}}});
      this.updateState({files: (files) => {
        if(0 !== files.size) {
          files.clear();
        }
      }});
    }
  }
  
  onActionCodeEditorFileNew(action) {
    this.sendDataAction(new DataActionCodeEditorFileNew(action));
  }
  
  onActionCodeEditorFileAdd(action) {
    this.updateState(this._exec(action.projectId, (project) => {
      const node = project.addFile(action.title, action.path, action.type);
      this.sendDataAction(new DataActionCodeEditorFileGet(`${node.data.path}/${node.title}`, action.projectId, node.key, node.title, node.data.path, node.data.type));
    }));
  }
  
  onActionCodeEditorFileRename(action) {
    this.sendDataAction(new DataActionCodeEditorFileRename(`${action.path}/${action.title}`, `${action.path}/${action.newTitle}`, action));
  }
  
  onActionCodeEditorFileUpdate(action) {
    const file = this.state.files.get(action.key);
    this.sendDataAction(new DataActionCodeEditorFileUpdate(action.key, file.title, file.path, file.content));
  }
  
  onActionCodeEditorFileUpdateAll(action) {
    action.keys.forEach((key) => {
      const file = this.state.files.get(key);
      this.sendDataAction(new DataActionCodeEditorFileUpdate(key, file.title, file.path, file.content));
    });
  }
    
  onActionCodeEditorFileEdit(action) {
    const file = this.state.files.get(action.key);
    this.updateState({current: {file: { content: {$set: action.content}}}});
    this.updateState({current: {file: { codeChanged: {$set: action.content !== file.contentOriginal}}}});
    this.updateState({files: (files) => {
      files.set(action.key, this.state.current.file);
    }});
  }
  
  onActionCodeEditorFileClose(action) {
    const file = this.state.files.get(action.key);
    const nextFile = this._getNextFile(file);
    this.updateState({files: (files) => {
      files.delete(action.key);
    }});
    if(undefined !== nextFile) {
      this.updateState({current: {file: {$set: nextFile}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FILE}}});
      action.history(this._generateQuery(nextFile.path, nextFile.title), {replace: true});
    }
    else {
      const node = this._getNode(file.path);
      const folder = this._createFolder(node.projectId, node.node);
      this.updateState({current: {file: {$set: null}}});
      this.updateState({current: {folder: {$set: folder}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
      action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
    }
  }
  
  onActionCodeEditorFileRemove(action) {
    const file = this.state.files.get(action.key) || this.state.current.file;
    const nextFile = this._getNextFile(file);
    this.updateState({files: (files) => {
      files.delete(action.key);
    }});
    this.updateState(this._exec(action.projectId, (project) => {
      project.removeNode(file.path, file.key);
    }));
    if(undefined !== nextFile) {
      this.updateState({current: {file: {$set: nextFile}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FILE}}});
      action.history(this._generateQuery(nextFile.path, nextFile.title), {replace: true});
    }
    else {
      const node = this._getNode(file.path);
      const folder = this._createFolder(node.projectId, node.node);
      this.updateState({current: {file: {$set: null}}});
      this.updateState({current: {folder: {$set: folder}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
      action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
    }
  }
  
  onActionCodeEditorFileDelete(action) {
    const file = this.state.files.get(action.key);
    this.sendDataAction(new DataActionCodeEditorFileDelete(`${file.path}/${file.title}`, action));
  }
  
  onActionCodeEditorFolderGet(action) {
    const node = this.state.project.findNode(`${action.path}/${action.title}` );
    const folder = this._createFolder(action.projectId, node);
    this.updateState({current: {folder: {$set: folder}}});
    this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
    action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
  }
  
  onActionCodeEditorFolderNew(action) {
    this.sendDataAction(new DataActionCodeEditorFolderNew(action));
  }
  
  onActionCodeEditorFolderAdd(action) {
    let node;
    this.updateState(this._exec(action.projectId, (project) => {
      node = project.addFolder(action.title, action.path, action.types);
    }));
    let folder = this._createFolder(action.projectId, node);
    this.updateState({current: {folder: {$set: folder}}});
    this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
    action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
  }
  
  onActionCodeEditorFolderUpdate(action) {
    this.sendDataAction(new DataActionCodeEditorFolderUpdate(`${action.path}/${action.title}`, `${action.path}/${action.newTitle}`, action));
  }
  
  onActionCodeEditorFolderRemove(action) {
    const node = this._getNode(`${action.path}/${action.title}`);
    const children = this.state.project.getAllFileChildren(node.node);
    
    children.forEach((child) => {
      const file = this.state.files.get(child.key);
      if(undefined !== file) {
        this.onActionCodeEditorFileClose({ key: child.key});
      }
    });
    
    const nodeParent = this._getNode(node.node.data.path);
    const folder = this._createFolder(nodeParent.projectId, nodeParent.node);
    
    this.updateState(this._exec(action.projectId, (project) => {
      project.removeNode(action.path, action.key);
    }));
    
    this.updateState({current: {folder: {$set: folder}}});
    this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
    action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
  }
  
  onActionCodeEditorFolderDelete(action) {
    this.sendDataAction(new DataActionCodeEditorFolderDelete(action));
  }
  
  onActionCodeEditorFileMove(action) {
    let found = false;
    let foundFromKey = false;
    let foundToKey = false;
    let newMap = new Map();
    let fromFile = null;
    this.state.files.forEach((file, key) => {
      if(!found) {
        if(action.fromKey === key) {
          foundFromKey = true;
          found = true;
        }
        else if(action.toKey === key) {
          fromFile = this.state.files.get(action.fromKey);
          newMap.set(action.fromKey, fromFile);
          newMap.set(action.toKey, file);
          foundToKey = true;
          found = true;
        }
        else {
          newMap.set(key, file);
        }
      }
      else {
        if(foundFromKey) {
          if(action.toKey === key) {
            newMap.set(action.toKey, file);
            fromFile = this.state.files.get(action.fromKey);
            newMap.set(action.fromKey, fromFile);
          }
          else if(action.fromKey !== key) {
            newMap.set(key, file);
          }
        }
        else if(foundToKey) {
           if(action.fromKey !== key) {
            newMap.set(key, file);
          }
        }
      }
    });
    this.updateState({files: {$set: newMap}});
    this.updateState({current: {file: {$set: fromFile}}});
    this.updateState({current: {type: {$set: CodeEditorStore.FILE}}});
    action.history(`/${fromFile.path}/${fromFile.title}`, {replace: true});
  }

  onActionCodeEditorProjectGet(action) {
    this.sendDataAction(new DataActionCodeEditorProjectGet(action.url, action.query));
  }
  
  onActionCodeEditorProjectUpdate(action) {
    if(!this.state.project.isSaved()) {
      this.sendDataAction(new DataActionCodeEditorProjectUpdate(this.state.project.projectId, this.state.project.source));
    }
  }
  
  onActionCodeEditorProjectToggle(action) {
    this.updateState(this._exec(action.projectId, (project) => {
      project.toggle(action.key, action.expanded);
    }));
  }
  
  onActionCodeEditorFileShowInvisibles(action) {
    this.updateState({showInvisibles: {$set: action.showInvisibles}});
  }
  
  onActionActorEditorBuild(action) {
    this.sendDataAction(new DataActionCodeEditorBuild(this.state.current.file.content, this.state.current.file.path, this.state.current.file.title));
  }

  onDataActionCodeEditorWorkspaceGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({current: {workspace: {$set: new Map(response.data.projects)}}});
      this.updateState({current: {settings: {$set: response.data.settings}}});
    }
  }
  
  onDataActionCodeEditorWorkspaceNew(action) {
    const response = action.getResponse();
    //console.log('onDataActionCodeEditorWorkspaceNew', response.result, response.data);
    if(response.isSuccess()) {
      //this.updateState({current: {workspace: {$set: new Map(response.data)}}});
      //console.log('aaaa', response.data);
    }
  }
  
  onDataActionCodeEditorFileGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let file = {
        projectId: action.projectId,
        key: action.key,
        title: action.title,
        path: action.path,
        type: action.type,
        content: response.data,
        contentOriginal: response.data,
        codeChanged: false,
        codeBuilt: false
      };
      this.updateState({files: (files) => { files.set(action.key, file); }});
      this.updateState({current: {file: {$set: file}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FILE}}});
      action.history(this._generateQuery(action.path, action.title), {replace: true});
    }
    else {
      this.updateState(this._exec(action.projectId, (project) => {
        let node = project.findNode(`${action.path}/${action.title}`);
        node.data._valid = false;
      }));
      let file = {
        projectId: action.projectId,
        key: action.key,
        title: action.title,
        path: action.path,
        type: action.type,
        codeChanged: false,
        codeBuilt: false
      };
      this.updateState({current: {file: {$set: file}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FILE_INVALID}}});
      action.history(this._generateQuery(action.path, action.title, {replace: true}));
    }
  }
  
  onDataActionCodeEditorFileNew(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState(this._exec(action.projectId, (project) => {
        let node = project.addFile(response.data, action.path, action.type);
        this.sendDataAction(new DataActionCodeEditorFileGet(`${node.data.path}/${node.title}`, action.projectId, node.key, node.title, node.data.path, node.data.type));
      }));
    }
  }
  
  onDataActionCodeEditorFileRename(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let node = this._getNode(`${action.path}/${action.title}`);
      this.updateState(this._exec(node.projectId, (project) => {
        project.renameFile(node.node, action.newTitle);
      }));
      let file = this.state.files.get(node.node.key);
      let updatedFile = this.shallowCopy(file);
      updatedFile.title = action.newTitle;
      this.updateState({files: (files) => {
        file = files.set(node.node.key, updatedFile);
      }});
      this.updateState({current: {file: {$set: updatedFile}}});
      action.history(this._generateQuery(updatedFile.path, updatedFile.title), {replace: true});
    }
  }
  
  onDataActionCodeEditorFileUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let file = this.state.files.get(action.key);
      if(undefined !== file) {
        let updatedFile = this.shallowCopy(file);
        updatedFile.codeChanged = false;
        updatedFile.contentOriginal = action.content;
        this.updateState({files: (files) => {
          files.set(action.key, updatedFile);
        }});
        if(this.state.current.file.key === action.key) {
          this.updateState({current: {file: {$set: updatedFile}}});
        }        
      }
    }
  }
  
  onDataActionCodeEditorFileDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.onActionCodeEditorFileRemove(action);
    }
  }
  
  onDataActionCodeEditorFolderNew(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let node;
      this.updateState(this._exec(action.projectId, (project) => {
        node = project.addFolder(action.title, action.path, action.types);
      }));
      let folder = this._createFolder(action.projectId, node);
      this.updateState({current: {folder: {$set: folder}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
      action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
    }
  }
  
  onDataActionCodeEditorFolderUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let node = this._getNode(`${action.path}/${action.title}`);
      let children = this.state.project.getAllFileChildren(node.node);
      this.updateState(this._exec(node.projectId, (project) => {
        project.renamePathRecursive(node.node, action.newTitle);
      }));
      node = this._getNode(`${action.path}/${action.newTitle}`);
      let folder = this._createFolder(node.projectId, node.node);
      this.updateState({current: {folder: {$set: folder}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
      children.forEach((child) => {
        let file;
        this.updateState({files: (files) => {
          file = files.get(child.key);
          if(undefined !== file) {
            file = this.shallowCopy(file);
            file.path = child.data.path;
            files.set(child.key, file);
          }
        }});
        if(this.state.current.file && this.state.current.file.key === child.key) {
          this.updateState({current: {file: {$set: file}}});
        }
      });
      action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
    }
  }
  
  onDataActionCodeEditorFolderDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.onActionCodeEditorFolderRemove(action);
    }
  }
  
  onDataActionCodeEditorProjectGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({project: (project) => { project.set(response.data); }});
    }
    if(undefined !== action.url) {
      const node = this._getNode(action.url);
      if(!node.node.folder) {
        this.sendDataAction(new DataActionCodeEditorFileGet(action.url, node.projectId, node.node.key, node.node.title, node.node.data.path, node.node.data.type));
      }
      else {
        const folder = this._createFolder(node.projectId, node.node);
        this.updateState({current: {folder: {$set: folder}}});
        this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
      }
    }
  }
  
  onDataActionCodeEditorProjectUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState(this._exec(action.projectId, (project) => {
        project.setSaved();
      }));
    }
  }
  
  _getNextFile(file) {
    let next;
    let mapIter = this.state.files.values();
    let iter = mapIter.next();
    while(!iter.done) {
      if(iter.value.key === file.key) {
        if(undefined !== next) {
          return next;
        }
        else {
          return mapIter.next().value;
        }  
      }
      next = iter.value;
      iter = mapIter.next();
    }
  }
  
  _getNode(file) {
    let node = this.state.project.findNode(file);
    if(undefined !== node) {
      return {
        projectId: this.state.project.projectId,
        node: node
      };
    }
  }
  
  _createFolder(projectId, node) {
    return {
      projectId: projectId,
      key: node.key,
      title: node.title,
      data: {
        path: node.data.path,
        types: node.data.types
      }
    };
  }
  
  _exec(projectId, exec) {
    if(projectId === this.state.project.projectId) {
      return {project: exec};
    }
  }
  
  _createQuery(url, search) {
    const query = {};
    if(search && search.length > 1) {
      const searchs = search.substring(1).split('&');
      if(searchs && searchs.length >= 1) {
        searchs.forEach((searchPart) => {
          const queryParam = searchPart.split('=');
          if(2 === queryParam.length) {
            Reflect.set(query, queryParam[0], queryParam[1]);
          }
        });
      }
    }
    if(undefined !== query && undefined !== query.line) {
      return {
        url: url,
        line: Number.parseInt(query.line),
        type: query.type ? Number.parseInt(query.type) : 0
      }
    }
  }
  
  _generateQuery(path, title) {
    const url = `${path}/${title}`
    const query = this.state.current.query;
    if(query && query.url && query.url === url) {
      let queries = [];
      let resultQuery = '';
      if(query.line) {
        queries.push(`line=${query.line}`);
      }
      if(query.type) {
        queries.push(`type=${query.type}`);
      }
      if(0 !== queries.length) {
         resultQuery = `?${queries[0]}`;
      }
      for(let i = 1; i < queries.length; ++i) {
        resultQuery += `&${queries[i]}`;
      }
      return `${url.substring(1)}${resultQuery}`;
    }
    else {
      return url.substring(1);
    }
  }
}

CodeEditorStore.FOLDER = 'folder';
CodeEditorStore.FILE = 'file';
CodeEditorStore.FILE_INVALID = 'invalid_file';


module.exports = new CodeEditorStore();
